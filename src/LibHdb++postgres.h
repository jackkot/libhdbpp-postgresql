/*
Source code file src/LibHdb++postgres.h
Part of PostgreSQL backend for HDB++ 
Description: Declaration of HdbPPpostgres class

Authors: Vladimir Sitnov, Alexander Senchenko, George Fatkin
Licence: see LICENCE file
*/

#ifndef _HDBPP_POSTGRES_H
#define _HDBPP_POSTGRES_H

#include <hdb++/AbstractDB.h>

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <queue>
#include <stdint.h>
#include <unordered_map>
#include <typeindex>
//Tango:
#include <tango.h>

// TODO: UNDEFINE IN HEADER!!!!!!!! IT IS NOT OK!!!!!!
// omg, tango defines CONNECTION_OK, just as libpq do. but I don't need one from tango. so I can undefine it before including libpq
#undef CONNECTION_OK
#include <libpq-fe.h>
#include <libpqtypes.h>
//#include <event.h>
#include "pgconverttango.h"

namespace hdbpp
{

struct PGType {
	std::string pg_type;
	unsigned int size;

};

class HdbPPpostgres: public AbstractDB {
private:
	PGconn *pgconn;
	bool lightschema;	//without recv_time and insert_time
	bool autodetectschema;
	std::map<std::string, uint32_t> attr_ID_map;
	std::map<std::string, bool> table_column_map;
	std::map<std::string, uint32_t> attr_ERR_ID_map;
	std::queue<std::string> attr_ERR_queue;
	std::unordered_map<std::type_index, std::string> type_map;

	void string_vector2map(const std::vector<std::string> &str, const std::string &separator, std::map<std::string, std::string> &results);
	void string_explode(const std::string &exstr, const std::string &separator, std::vector<std::string> &results);
	std::string get_only_tango_host(const std::string &str);
	int find_attr_id(const std::string &facility, const std::string &attr, uint32_t &ID);
	std::string get_only_attr_name(const std::string &str);
	int find_last_event(uint32_t ID, std::string &event);
	std::string get_table_name(int type/*DEV_DOUBLE, DEV_STRING, ..*/, int format/*SCALAR, SPECTRUM, ..*/, int write_type/*READ, READ_WRITE, ..*/);
	std::string get_data_type(int type/*DEV_DOUBLE, DEV_STRING, ..*/, int format/*SCALAR, SPECTRUM, ..*/, int write_type/*READ, READ_WRITE, ..*/);
	int find_attr_id_type(const std::string &facility, const std::string &attr, uint32_t &ID, const std::string &attr_type, unsigned int &conf_ttl);
	void event_Attr(uint32_t id, const std::string &event);
	bool autodetect_column(int type/*DEV_DOUBLE, DEV_STRING, ..*/, int format/*SCALAR, SPECTRUM, ..*/, int write_type/*READ, READ_WRITE, ..*/, const std::string &column_name);
	int cache_err_id(const std::string &error_desc, uint32_t &ERR_ID);
	int insert_error(const std::string &error_desc, uint32_t &ERR_ID);
	int find_err_id(const std::string &err, uint32_t &ERR_ID);
	void store_scalar(const std::string &attr, int quality/*ATTR_VALID, ATTR_INVALID, ..*/, const std::string &error_desc, double ev_time, double rcv_time, const std::string &table_name, PGConvertTango *conv);
	void store_array(const std::string &attr, int quality/*ATTR_VALID, ATTR_INVALID, ..*/, const std::string &error_desc, Tango::AttributeDimension attr_r_dim, Tango::AttributeDimension attr_w_dim, double ev_time, double rcv_time, const std::string &table_name, PGConvertTango *conv);
#ifndef _MULTI_TANGO_HOST
	std::string remove_domain(const std::string &facility);
	std::string add_domain(const std::string &facility);
#endif


public:
	HdbPPpostgres(const string &id,  const vector<string> &configuration);
	~HdbPPpostgres();

	// Inserts an attribute archive event for the EventData into the database. If the attribute
	// does not exist in the database, then an exception will be raised. If the attr_value
	// field of the data parameter if empty, then the attribute is in an error state
	// and the error message will be archived.
	void insert_event(Tango::EventData *event_data, const HdbEventDataType &data_type) override;

	// Insert multiple attribute archive events. Any attributes that do not exist will
	// cause an exception. On failure the fall back is to insert events individually
	void insert_events(std::vector<std::tuple<Tango::EventData *, HdbEventDataType>> events) override;

	// Inserts the attribute configuration data (Tango Attribute Configuration event data)
	// into the database. The attribute must be configured to be stored in HDB++,
	// otherwise an exception will be thrown.
	void insert_param_event(Tango::AttrConfEventData *param_event, const HdbEventDataType & /* data_type */) override;

	// Add an attribute to the database. Trying to add an attribute that already exists will
	// cause an exception
	void add_attribute(const std::string &fqdn_attr_name, int type, int format, int write_type) override;

	// Update the attribute ttl. The attribute must have been configured to be stored in
	// HDB++, otherwise an exception is raised
	void update_ttl(const std::string &fqdn_attr_name, unsigned int ttl) override;

	// Inserts a history event for the attribute name passed to the function. The attribute
	// must have been configured to be stored in HDB++, otherwise an exception is raised.
	// This function will also insert an additional CRASH history event before the START
	// history event if the given event parameter is DB_START and if the last history event
	// stored was also a START event.
	void insert_history_event(const std::string &fqdn_attr_name, unsigned char event) override;

	// Check what hdbpp features this library supports. This library supports: TTL, BATCH_INSERTS
	bool supported(HdbppFeatures feature) override;
};

class HdbPPpostgresFactory : public DBFactory {
public:
	virtual AbstractDB* create_db(const string &id, const vector<string> &configuration);
};

} // namespace hdbpp
#endif